---
# SPDX-License-Identifier: CC-BY-4.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
---

