---
# SPDX-License-Identifier: CC-BY-4.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
title: "Home"
layout: article
recent: date
nrecent: 30
nlast: 10
autonumbering: true
toc: true
---


# Project Layout

The project layout is designed for research applications in scientific computing.

```bash
/
├── author.json
├── bench
│   ├── acceptance
│   ├── CMakeLists.txt
│   ├── common
│   ├── quality
│   ├── smoke
│   └── micro
├── bs
│   └── options.cmake
├── .clang-format
├── .clang-tidy
├── cmake
│   └── vorlageConfig.cmake.in
├── .cmake-format.py
├── CMakeLists.txt
├── CMakePresets.json
├── dev
│   ├── bin
│   ├── doc
│   ├── etc
│   └── lib
├── doc
│   └── doxygen
├── .gitignore
├── .gitlab-ci.yml
├── lab
│   └── CMakeLists.txt
├── readme.md
├── showcase
│   └── CMakeLists.txt
├── src
│   ├── CMakeLists.txt
│   ├── c
│   ├── cpp
│   ├── cuda
│   ├── hip
│   └── protobuf
└── test
    ├── acceptance
    ├── CMakeLists.txt
    ├── common
    ├── integration
    ├── quality
    ├── sanity
    └── unit
```

## `author.json`

The purpose of this file is to record the names and contact details of each of
the contributors to the project. Each contributor will receive a unique ID, so
that similar names do not get confused. Keeping data in this manner combined
with a couple of scripts can verify that the copyright informations in the file
and the git history are consistent and can simplify copyright management of
open-source projects.

A typical entry in the project will
look like:

```json
{
  "projectid" : "project_specific_unique_id",
  "name" : "Author's Name",
  "email" : [
    "somemail@host.com"
  ],
  "other-method-example-gitlab-id" : [
    "gitlabid1",
  "gitlabid2"
  ]
}
```

## `bench`

The sub-directory structure is:
```bash
/
├── acceptance
├── baseline
├── CMakeLists.txt
├── common
├── quality
└── micro
```

- `micro`

    `micro` directory contains the microbenchmarks.

- `integration`

    `integration` directory contains the more comprehensive benchmarks which
    cover more components of the code but still might not provide any `quality`
    metrics for the code.

- `quality`

    The `quality` benchmarks as the name suggests are benchmarks which evaluate
    quality metrics of the application. They can be used to demonstrate the
    performance of the application in different areas of the code.


- `acceptance`

    The `acceptance` benchmarks as the name suggests are benchmarks which
    evaluate acceptance metrics of the application. A failure of `acceptance`
    benchmark to meet the performance requirements would fail the build.
    `acceptance` benchmarks are usually subset of `quality` benchmarks for
    specific use-cases.

- `baseline`

    The `baseline` benchmarks as the name suggests are baseline benchmarks which
    can be used to calibrate the systems and the benchmark runs.

    **Justification**
    Most of these benchmarks (STREAM, IOR for example) are often
    application-independent, and therefore ideally, are more suited in
    repositories of their own. However, every once in a while, a benchmark has
    just enough characteristics very specific to the application despite not
    depending on any of the application code. This is especially true when
    performance regression investigations are in process, and there is an
    effort to reduce the application to a minimal working example without any
    application code. The occurence of such scenarios is often enough to
    provide for the `baseline` benchmark.


## `bs`

`bs` directory contains the build scripts. The layout only requires the
`options.cmake` build script, but it can contain other files as well.

- `options.cmake` :
  `options.cmake` is a cmake file which contains  all the options that the
  build script recognizes. A physical separation of the file from the rest of
  the build script makes it easy for old and new contributors to verify the
  correctness of build documentation against actual code.

  An example of text in build documentation is as follows:

  ```cmake
  # BUILD_DOC (Option)
  # If enabled, the build will also build the documentation to go along with the
  # software. The build documentation is stored under the `doc` sub-directory in
  # the build directory, and the documentation will be installed in
  # `<prefix>/share/vorlage/doc`.
  option(BUILD_DOC "Build Documentation" Off)
  ```

## `cmake`

This directory contains all the cmake files that `cmake` has to process to
generate the build. An example of such a file is `vorlageConfig.cmake.in` which
is processed using the `configure_package_config_file` command.

## `dev`

`dev` directory is for developers to store various scripts and information that
are only important to development process. An example of such information is
example editor/IDE configurations that the developer might want to use with
this project, or scripts to verify author/copyright information and other such
things.

## `doc`

`doc` directory contains documentation for the software. Even if the software
has a separate documentation website with its own repository, there are still
some things that need to be stored inside the repository. An example of this is
the `doxygen` templates for the complete code.

## `lab`

At any given point in time, a research software in scientific will have
multiple experiments in flight. Managing this experiments is a tricky business
because the maintainers will be reluctant to ship these experiments as a part
of the release, and yet, it makes sense to integrate them in the main
development tree of the code to prevent breakages of codes in future.

`lab` directory contains all the multiple experiments going on in the
application as their own `mini-apps` complete with their own tests and
benchmarks. There are two advantages of this approach:
1. The maintainers can choose to run the tests and benchmarks in the `lab` as a
   part of their `CI` but then allow the tests and benchmarks to fail. This
   allows everyone involved to be aware of the breakages that are caused, and
   help to fix them in future.

2. Someone working on a feature in `lab` can choose to run the CI only for the
   specific `lab` during their initial development and therefore avoid the cost
   of running the full application CI for experiments that do not touch the
   main core of the code.

3. There can be much larger freedom in the `lab` to have dependencies and
   compile options which might not be desirable to include just yet in the main
   code of the software.


## `showcase`

Over the course of its lifetime, a research software in scientific computing
will need to publish a collection of results. It is then important for other
people to be able to replicate the findings. Often the path from actually
running the test to generate the final output reports can require longer
pipelines. Further, the software required to generate the reports might have
dependencies which do not make sense to be included as general dependencies.
Finally, some of the showcase experiments can be extremely expensive and it
might make sense to run them only once a year or so to verify that they work.

All of these motivate a separate directory which contains all the code required
to reproduce or replicate the showcase reports for the software.

## `src`

This is the source code directory which contains the core source code of the
application. It has a directory structure like follows:

```bash
src
├── c
├── cpp
├── cuda
├── hip
├── python
└── protobuf
```

Depending on the platform and the site you are running code in, you might or
might want want to enable support for different languages. Because different
compilers might be used to process different codes, it makes sense to separate
the code by tools used to compile the code. The source code used to generate
more source code (for example `protobuf`) also finds a place here.

We have specifically not mentioned how to do project layout for each language,
because in each tool/language-specific directory, the layout is dictated by the
language models. For exameple, for `C` and `C++`, you might want a
`include`,`bin` and  `lib` directories for headers, executables and library
code, but for `python`  you only might want `bin` and `lib` directories, while
for code generators like `protobuf` and/or `Qt/Moc` you might want to organize
the sources by whatever the tools expect there.


## `test`

```bash
test
├── acceptance
├── CMakeLists.txt
├── common
├── integration
├── quality
├── sanity
└── unit
```
- `sanity`: sanity tests

    These tests check whether the tests themselves are working correctly. For
    example, if you are running an address sanitizer, then a sanity test will
    run which will trigger the address sanitizer and fail, and the test runner
    will expect and detect that failure.
    This saves us from situations where we get false negatives in the sanitizer
    CI due to some misconfigurations in the builds or the toolchains.

- `unit` : `unit` tests
- `integration` : `integration` tests
- `quality` and `acceptance` : same function as benchmarks
